import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import 'rxjs/add/operator/filter';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit {

    constructor(
        private router: Router
    ) {}

    ngOnInit() {
        this.router.events
        .filter( event => event instanceof NavigationEnd )
        .subscribe( event => {
            /*console.log( event );
            if ( event instanceof NavigationEnd ) {
                console.log( event );
            }*/

            console.log( event );
        });
    }
}
