import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AuthModule } from '../auth/auth.module';
import { AuthGuard } from '../auth/auth.guard';
import { MailFolderComponent } from './containers/mail-folder/mail-folder.component';
import { MailAppComponent } from './components/mail-app/mail-app.component';
import { MailItemComponent } from './components/mail-item/mail-item.component';
import { MailService } from './mail.service';
import { MailFolderResolver } from './containers/mail-folder/mail-folder.resolve';
import { MailViewResolve } from './components/mail-view/mail-view.resolve';
import { MailViewComponent } from './components/mail-view/mail-view.component';
import { MailViewGuard } from './components/mail-view/mail-view.guard';


export const MAIL_ROUTES: Routes = [
    {
        path: 'mail',
        component: MailAppComponent,
        canActivateChild: [
            AuthGuard
        ],
        children: [
            {
                path: 'folder/:name',
                component: MailFolderComponent,
                resolve: {
                    messages: MailFolderResolver
                }
            },
            {
                path: 'message/:id',
                component: MailViewComponent,
                outlet: 'single-message',
                canDeactivate: [ MailViewGuard ],
                resolve: {
                    message: MailViewResolve
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        CommonModule,
        AuthModule,
        RouterModule.forChild( MAIL_ROUTES )
    ],
    declarations: [
        MailFolderComponent,
        MailAppComponent,
        MailItemComponent,
        MailViewComponent
    ],
    exports: [
        MailAppComponent
    ],
    providers: [
        MailService,
        MailFolderResolver,
        MailViewResolve,
        MailViewGuard
    ]
})


export class MailModule { }
