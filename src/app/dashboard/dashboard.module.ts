import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './containers/dashboard/dashboard.component';


export const DASHBOARD_ROUTES: Routes = [
    {
        path: '',
        component: DashboardComponent
    }

];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild( DASHBOARD_ROUTES )
    ],
    declarations: [
        DashboardComponent
    ]
})


export class DashboardModule { }
