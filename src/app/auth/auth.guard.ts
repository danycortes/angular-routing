import { Injectable } from '@angular/core';
import { CanLoad, CanActivate, CanActivateChild } from '@angular/router';

import { AuthService } from './auth.service';


@Injectable()

// Function call before router gets executed, to check if the user has permissions to get an specific route
export class AuthGuard implements CanLoad, CanActivate, CanActivateChild {

    constructor(
        private authService: AuthService
    ) {}

    canLoad() {
        return this.authService.checkPermissions();
    }

    canActivate() {
        return this.authService.isLoggedIn();
    }

    canActivateChild() {
        return true;
    }
}
