import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Route, Routes, PreloadingStrategy } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { AppComponent } from './app.component';
import { MailModule } from './mail/mail.module';
import { AuthModule } from './auth/auth.module';
import { AuthGuard } from './auth/auth.guard';


export const ROUTES: Routes = [
    { path: 'dashboard', canLoad: [ AuthGuard ], loadChildren: './dashboard/dashboard.module#DashboardModule', data: { preload: true } },
    { path: '**', redirectTo: 'mail/folder/inbox' }
];

export class CustomPreloadModules implements PreloadingStrategy {
    preload( route: Route, fn: () => Observable<any> ): Observable<any> {
        return route.data && route.data.preload ? fn() : Observable.of( null );
    }
}



@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        MailModule,
        AuthModule,
        RouterModule.forRoot( ROUTES, { preloadingStrategy: CustomPreloadModules } )
    ],
    providers: [
        CustomPreloadModules,
        AuthGuard
    ],
    bootstrap: [
        AppComponent
    ]
})


export class AppModule { }
